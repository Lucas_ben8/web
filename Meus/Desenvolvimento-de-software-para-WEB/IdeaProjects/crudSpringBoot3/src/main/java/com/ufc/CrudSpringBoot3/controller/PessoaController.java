package com.ufc.CrudSpringBoot3.controller;

import com.ufc.CrudSpringBoot3.model.Pessoa;
import com.ufc.CrudSpringBoot3.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @RequestMapping("/pessoa/formulario")
    public ModelAndView formularioPessoa(){
        ModelAndView mv = new ModelAndView("formulario");
        //mv.addObject("pessoa", new Pessoa());
        return mv;
    }

    @PostMapping("/pessoa/salvar")
    public ModelAndView salvar(Pessoa pessoa, @RequestParam(value = "imagem")MultipartFile imagem){
        pessoaService.salvar(pessoa, imagem);
        ModelAndView mv = new ModelAndView("formulario");
        mv.addObject("mensagem", "O Usuário foi cadastrado com sucesso!");
        return mv;
    }

    @RequestMapping("/pessoa/listar")
    public ModelAndView listagem(){
        List<Pessoa> pessoas = pessoaService.retornarLista();
        ModelAndView mv =  new ModelAndView("pagina-listagem");
        mv.addObject("todasAsPessoas", pessoas);
        return mv;
    }

    @RequestMapping("/pessoa/excluir/{id}")
    public ModelAndView exluirPessoa(@PathVariable Long id){
        pessoaService.excluirPessoa(id);
        ModelAndView mv = new ModelAndView("redirect:/pessoa/listar");
        return mv;
    }

    @RequestMapping("/pessoa/atualizar/{id}")
    public ModelAndView atualizarPessoa(@PathVariable Long id){
        Pessoa pessoa = pessoaService.buscaPorId(id);
        ModelAndView mv = new ModelAndView("formulario");
        mv.addObject("pessoa", pessoa);
        return mv;
    }

    @RequestMapping("/pessoa/logar")
    public String logar(){
        return "login";
    }

    @RequestMapping("/pessoa/logout")
    public ModelAndView logout(){
        ModelAndView mv = new ModelAndView("login");
        mv.addObject("mensagem", "O usuario deslogou-se");
        return mv;
    }

}
