package com.ufc.CrudSpringBoot3.config;

import com.ufc.CrudSpringBoot3.Security.UserDetailsServiceImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImplement userDetailsServiceImplement;

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.csrf().disable().authorizeRequests()
                .antMatchers("/inicio").permitAll()
                .antMatchers("/pessoa/formulario").hasRole("USER")
                .antMatchers("/pessoa/salvar").hasAnyRole("USER", "ADMIN")
                .antMatchers("/pessoa/listar").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/pessoa/logar").permitAll()
                .and().logout().logoutSuccessUrl("/pessoa/logar?logout").permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(userDetailsServiceImplement).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) throws Exception{
        web.ignoring().antMatchers("/css/**", "/js/**");
    }



}
