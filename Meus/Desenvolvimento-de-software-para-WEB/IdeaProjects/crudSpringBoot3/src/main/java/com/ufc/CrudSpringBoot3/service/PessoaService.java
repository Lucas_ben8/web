package com.ufc.CrudSpringBoot3.service;

import com.ufc.CrudSpringBoot3.model.Pessoa;
import com.ufc.CrudSpringBoot3.repository.PessoaRepository;
import com.ufc.CrudSpringBoot3.util.ArquivoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    public void salvar(Pessoa pessoa, MultipartFile imagem){
        pessoa.setSenha(new BCryptPasswordEncoder().encode(pessoa.getSenha()));
        String caminho = "imagens/" + pessoa.getNome() + ".png";
        ArquivoUtil.salvarImagem(caminho, imagem);
        pessoaRepository.save(pessoa);
    }

    public List<Pessoa> retornarLista() {
        return pessoaRepository.findAll();
    }

    public void excluirPessoa(Long id) {//gostaria de fazer boleano
        pessoaRepository.deleteById(id);
    }

    public Pessoa buscaPorId(Long id) { // add regra de negossio bool
        return pessoaRepository.getOne(id);
    }
}
