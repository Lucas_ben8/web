package DAO;

import pojo.Pessoa;

import java.util.ArrayList;

public class PessoaDAO {
    ArrayList<Pessoa> pessoas = new ArrayList<>();

    public void addPessoas(){
        pessoas.add(new Pessoa("Ana", "0436"));
        pessoas.add(new Pessoa("Benjamim","3518"));
        pessoas.add(new Pessoa("Ambrozina", "2343"));
        pessoas.add(new Pessoa("Lucas", "0736"));
        pessoas.add(new Pessoa("Camila", "0838"));
    }

    public Pessoa buscarPessoa(String login, String senha){
        for (Pessoa pessoa: pessoas) {
            if(pessoa.getLogin().equals(login) && pessoa.getSenha().equals(senha)){
                System.out.println("pessoa encontrada");
                return pessoa;
            }
        }
        return null;
    }
}
