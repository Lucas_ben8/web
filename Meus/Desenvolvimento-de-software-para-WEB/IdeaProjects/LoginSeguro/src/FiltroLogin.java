import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class FiltroLogin implements Filter {
    public FiltroLogin() {
        super();
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        HttpServletResponse httpServletResponse = (HttpServletResponse) resp;
        HttpSession sessao = httpServletRequest.getSession();

        if(sessao.getAttribute("UsuarioLogado") != null ||
                httpServletRequest.getRequestURI().endsWith("FormLogin") ||
                httpServletRequest.getRequestURI().endsWith("EfetuarLogin") ||
                httpServletRequest.getRequestURI().endsWith("PaginaErro") ||
                httpServletRequest.getRequestURI().contains("Paginas")){


            chain.doFilter(req, resp);
            return;
        }
        httpServletResponse.sendRedirect("FormLogin");



    }

    public void init(FilterConfig config) throws ServletException {

    }

}
