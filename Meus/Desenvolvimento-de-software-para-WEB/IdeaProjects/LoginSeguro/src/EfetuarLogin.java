import DAO.PessoaDAO;
import pojo.Pessoa;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/EfetuarLogin")
public class EfetuarLogin extends HttpServlet {

    public EfetuarLogin() {
        super();
    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession sessao = request.getSession();
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");

        PessoaDAO pessoaDAO = new PessoaDAO();
        pessoaDAO.addPessoas();
        Pessoa pessoa = pessoaDAO.buscarPessoa(login, senha);

        if(pessoa == null){
            response.sendRedirect("PaginaErro");
        }else{
            sessao.setAttribute("UsuarioLogado", login);
            response.sendRedirect("TelaInicial");
        }
    }

}
