import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

@WebServlet("/gerarNumeros")
public class GerarNumeros extends HttpServlet {
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList numeros = new ArrayList();
        for(int i = 1; i<=60; i++){
            numeros.add(i);
        }

        Collections.shuffle(numeros);

        PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<body>");
        out.println("<h1>");

        out.print("Seus numeros são: ");

        ArrayList certos = new ArrayList();

        for(int i = 0; i < 6; i++){
            certos.add(numeros.get(i));
        }

        Collections.sort(certos);

        for(int i = 0; i < 6; i++){
            out.print(certos.get(i) + " ");
        }
        out.println("</h1>");



        out.println("</body>");
        out.println("</html>");



    }

}
