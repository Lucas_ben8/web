package ufc.com.teste;

import ufc.com.dao.PistaDAO;
import ufc.com.dao.ProprietarioDAO;
import ufc.com.dao.VeiculoDAO;
import ufc.com.model.Pista;
import ufc.com.model.Proprietario;
import ufc.com.model.Veiculo;

public class TesteMapeamento {
    public static void main(String [] args){
        ProprietarioDAO proprietarioDAO = new ProprietarioDAO();
        VeiculoDAO veiculoDAO = new VeiculoDAO();
        PistaDAO pistaDAO = new PistaDAO();

        Proprietario proprietario = new Proprietario();
        proprietario.setNome("Ana Maria");
        proprietario.setTelefone("43246");
        proprietarioDAO.add(proprietario);

        Veiculo veiculo = new Veiculo();
        veiculo.setFabricante("VareNopala");
        veiculo.setModelo("Kambria");
        veiculo.setValor(1500430.60);
        veiculo.setProprietario(proprietario);
        veiculoDAO.adicionar(veiculo);

        Veiculo veiculo2 = new Veiculo();
        veiculo.setFabricante("Ximino");
        veiculo.setModelo("variasCoisas");
        veiculo.setValor(10430.60);
        veiculo.setProprietario(proprietario);
        veiculoDAO.adicionar(veiculo2);

        Pista pista = new Pista();
        pista.setLocal("Rio Negro");
        pista.setTamanho(5000);
        pista.setProprietario(proprietario);
        pistaDAO.addPista(pista);

        proprietario.setNome("vládia");
        proprietarioDAO.alterar(proprietario);
        proprietarioDAO.lista();
        //proprietarioDAO.deletar((long) 1);

        pista.setTamanho(4000);
        pistaDAO.alterar(pista);

        pistaDAO.lista();

        //pistaDAO.deletar((long) 1);

        veiculo2.setModelo("lfkdakdfls");
        veiculoDAO.alterar(veiculo2);
        veiculoDAO.listar();
        //veiculoDAO.deletar((long) 1);



    }
}
