package ufc.com.dao;

import ufc.com.model.Pista;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class PistaDAO {
    public void addPista(Pista pista){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        try{
            manager.getTransaction().begin();
            manager.persist(pista);
            manager.getTransaction().commit();
        }catch (Exception e){
            manager.getTransaction().rollback();
            e.printStackTrace();
        }finally {
            manager.close();
            fabrica.close();
        }
    }

    public void alterar(Pista pista){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        try{
            manager.getTransaction().begin();
            manager.merge(pista);
            manager.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
            manager.getTransaction().rollback();
        }finally {
            manager.close();
            fabrica.close();
        }
    }

    public void deletar(Long id){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        try{
            manager.getTransaction().begin();
            Pista pista = manager.find(Pista.class, id);
            manager.remove(pista);
            manager.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
            manager.getTransaction().rollback();
        }finally {
            manager.close();
            fabrica.close();
        }

    }

    public List<Pista> lista(){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        try{
            List<Pista> pistas = manager.createQuery("select p from Pista as p", Pista.class).getResultList();
            return pistas;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

}
