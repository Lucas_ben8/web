package ufc.com.dao;

import ufc.com.model.Veiculo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class VeiculoDAO {


    public void adicionar(Veiculo veiculo){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        try{
            manager.getTransaction().begin();
            manager.persist(veiculo);
            manager.getTransaction().commit();
        }catch (Exception e){
            manager.getTransaction().rollback();
            e.printStackTrace();
        }finally {
            manager.close();
            fabrica.close();
        }
    }

    public void alterar(Veiculo veiculo){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        manager.getTransaction().begin();
        manager.merge(veiculo);
        manager.getTransaction().commit();

        fabrica.close();
        manager.close();
    }

    public void deletar(Long id){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        manager.getTransaction().begin();
        Veiculo veiculo = manager.find(Veiculo.class, id);
        manager.remove(veiculo);
        manager.getTransaction().commit();
        fabrica.close();
        manager.close();
    }

    public List<Veiculo> listar(){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        List<Veiculo> veiculos = manager.createQuery("select v from Veiculo as v", Veiculo.class).getResultList();
        return veiculos;
    }

}
