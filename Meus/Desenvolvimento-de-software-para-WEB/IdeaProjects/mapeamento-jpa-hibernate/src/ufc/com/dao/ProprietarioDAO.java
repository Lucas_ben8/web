package ufc.com.dao;

import ufc.com.model.Proprietario;
import ufc.com.model.Veiculo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class ProprietarioDAO {


    public void add(Proprietario proprietario){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        try{
            manager.getTransaction().begin();
            manager.persist(proprietario);
            manager.getTransaction().commit();

        }catch (Exception e){
            manager.getTransaction().rollback();
            e.printStackTrace();
        }finally {
            fabrica.close();
            manager.close();
        }

    }

    public void alterar(Proprietario proprietario){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        try{
            manager.getTransaction().begin();
            manager.merge(proprietario);
            manager.close();
        }catch (Exception e){
            manager.getTransaction().rollback();
            e.printStackTrace();
        }finally {
            manager.close();
            fabrica.close();
        }
    }

    public void deletar(Long id){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();

        try{
            manager.getTransaction().begin();
            Proprietario proprietario = manager.find(Proprietario.class, id);
            manager.remove(proprietario);
            manager.getTransaction().commit();
        }catch (Exception e){
            manager.getTransaction().rollback();
            e.printStackTrace();
        }finally {
            manager.close();
            fabrica.close();
        }
    }

    public List<Proprietario> lista(){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("mapeamento-jpa-hibernate");
        EntityManager manager = fabrica.createEntityManager();
        try{
            List<Proprietario> proprietarios= manager.createQuery("select p from Proprietario as p", Proprietario.class).getResultList();
            return proprietarios;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
