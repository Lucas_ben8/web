package ufc.com.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Proprietario {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long id;

    private String nome;
    private String telefone;

    @OneToMany
    private List<Veiculo> veiculo;

    @OneToOne(mappedBy = "proprietario")
    private Pista pista;


    public List<Veiculo> getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(List<Veiculo> veiculo) {
        this.veiculo = veiculo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
