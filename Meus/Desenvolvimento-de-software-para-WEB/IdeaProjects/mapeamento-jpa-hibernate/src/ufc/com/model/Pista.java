package ufc.com.model;

import javax.persistence.*;

//cada proprietario tem uma pista // um para um
@Entity
public class Pista {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String local;
    private int tamanho;


    @OneToOne
    @JoinColumn
    private Proprietario proprietario;


    public Pista() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public Proprietario getProprietario() {
        return proprietario;
    }

    public void setProprietario(Proprietario proprietario) {
        this.proprietario = proprietario;
    }
}
