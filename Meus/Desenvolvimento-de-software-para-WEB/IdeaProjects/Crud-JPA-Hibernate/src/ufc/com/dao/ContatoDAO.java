package ufc.com.dao;

import ufc.com.modelo.Contato;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class ContatoDAO {
    public void adiciona(Contato contato){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("Crud-JPA-Hibernate");
        EntityManager manager = fabrica.createEntityManager();

        manager.getTransaction().begin();
        manager.persist(contato);
        manager.getTransaction().commit();
        fabrica.close();
        manager.close();
    }

    public void alterar(Contato contato){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("Crud-JPA-Hibernate");
        EntityManager manager = fabrica.createEntityManager();

        manager.getTransaction().begin();
        manager.merge(contato);
        manager.getTransaction().commit();
        fabrica.close();
        manager.close();
    }

    public void remove(Long id){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("Crud-JPA-Hibernate");
        EntityManager manager = fabrica.createEntityManager();

        Contato contato = manager.find(Contato.class, id);

        manager.getTransaction().begin();
        manager.remove(contato);
        manager.getTransaction().commit();
        fabrica.close();
        manager.close();

    }

    public Contato buscarPorId(Long id){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("Crud-JPA-Hibernate");
        EntityManager manager = fabrica.createEntityManager();

        Contato contato = manager.find(Contato.class, id);
        fabrica.close();
        manager.close();
        return contato;
    }

    public List<Contato> listar(){
        EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("Crud-JPA-Hibernate");
        EntityManager manager = fabrica.createEntityManager();

        List<Contato> contatos = manager.createQuery("select c from Contato as c", Contato.class).getResultList();
        fabrica.close();
        manager.close();
        return contatos;

    }
}
