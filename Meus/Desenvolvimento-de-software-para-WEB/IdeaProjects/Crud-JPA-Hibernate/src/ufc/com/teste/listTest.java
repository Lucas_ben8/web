package ufc.com.teste;

import ufc.com.dao.ContatoDAO;
import ufc.com.modelo.Contato;

import java.util.List;

public class listTest {
    public static void main(String [] args){
        ContatoDAO dao = new ContatoDAO();
        List<Contato> contatos = dao.listar();

        for (Contato contato: contatos) {
            System.out.println("Nome:" + contato.getNome());
            System.out.println("Telefone: " + contato.getTelefone());
            System.out.println("Id: " + contato.getId() + "\n");
        }
    }

}
