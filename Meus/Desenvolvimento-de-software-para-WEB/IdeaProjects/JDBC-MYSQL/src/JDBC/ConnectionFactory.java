package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    public Connection getConnection(){
        try{
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            return DriverManager.getConnection("jdbc:mysql://localhost/UFC", "lucasBen","Kanim@2621");
        }catch (SQLException sql){
            throw new RuntimeException(sql);
        }
    }
}
