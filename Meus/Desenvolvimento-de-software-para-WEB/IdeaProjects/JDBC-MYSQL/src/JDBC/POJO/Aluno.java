package JDBC.POJO;

import java.sql.Date;
//import java.util.Calendar;

public class Aluno {
    private long matricula;
    private String nome;
    private String endereco;
    private String email;
    private Date dataNasc;

    public Aluno(long matricula, String nome, String endereco, String email, Date dataNasc) {
        this.matricula = matricula;
        this.nome = nome;
        this.endereco = endereco;
        this.email = email;
        this.dataNasc = dataNasc;
    }

    public Aluno(String nome, String endereco, String email, Date dataNasc) {
        this.nome = nome;
        this.endereco = endereco;
        this.email = email;
        this.dataNasc = dataNasc;
    }

    public Aluno(){

    }

    public long getMatricula() {
        return matricula;
    }

    public void setMatricula(long matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }
}
