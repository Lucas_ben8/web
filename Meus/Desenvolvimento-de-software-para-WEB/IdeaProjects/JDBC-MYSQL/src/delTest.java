import JDBC.DAO.AlunoDAO;
import JDBC.POJO.Aluno;

import java.sql.Date;

public class delTest {
    public static void main(String[] args){
        Aluno aluno = new Aluno("Lucas Benjamim", "Joao Rocha", "Lucas.explorer2015@G", Date.valueOf("1997-08-02"));
        AlunoDAO alunoDAO = new AlunoDAO();
        aluno.setMatricula(1);
        alunoDAO.delAluno(aluno);
    }
}
