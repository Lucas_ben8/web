import JDBC.DAO.AlunoDAO;
import JDBC.POJO.Aluno;
import java.util.List;

public class listTest {
    public static void main(String[] args){
        AlunoDAO dao = new AlunoDAO();
        List<Aluno> alunos = dao.showAluno();

        for (Aluno aluno : alunos) {
            System.out.println("Nome: " + aluno.getNome());
            System.out.println("E-mail:" + aluno.getEmail());
            System.out.println("Endereço: " + aluno.getEndereco());
            System.out.println("Data de Nascimento: " + aluno.getDataNasc());
            System.out.println("Matrícula: " + aluno.getMatricula() + "\n");
        }
    }
}
