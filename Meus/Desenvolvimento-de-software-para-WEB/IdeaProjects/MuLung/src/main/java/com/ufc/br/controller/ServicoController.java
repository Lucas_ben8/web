package com.ufc.br.controller;

import com.ufc.br.Service.ServicoService;
import com.ufc.br.model.Servico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ServicoController{

    @Autowired
    private ServicoService servicoService;

    @RequestMapping("/servico/salvar")
    public ModelAndView salvar(Servico servico, @RequestParam(value = "imagemser")MultipartFile imagem){
        servicoService.salvar(servico, imagem);
        CadastrarProdutosController cadastrarProdutosController = new CadastrarProdutosController();
        return cadastrarProdutosController.cadastrar("Serviço cadastrado com Sucesso!");
    }

    @RequestMapping("/servico/editar/{id}")
    public ModelAndView edita(@PathVariable Long id){
        Servico servico = servicoService.buscarPorId(id);
        ModelAndView mv = new ModelAndView("editarServico");
        mv.addObject( "servico", servico);
        return mv;

    }

    @RequestMapping("/servico/excluir/{id}")
    public ModelAndView excluir(@PathVariable Long id){
        servicoService.excluirServico(id);
        ModelAndView mv = new ModelAndView("redirect:/Produtos ");
        return mv;
    }

}
