package com.ufc.br.Service;

import com.ufc.br.Repository.PlantaRepository;
import com.ufc.br.Util.ArquivoUtil;
import com.ufc.br.model.Ferramenta;
import com.ufc.br.model.Planta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class PlantaService {

    @Autowired
    private PlantaRepository plantaRepository;


    public void salvar(Planta planta, MultipartFile imagem) {
        String caminho = "Imagens/img/" + planta.getNome() + "_" + planta.getTipo() + ".png";
        ArquivoUtil.salvarImagem(caminho, imagem);
        plantaRepository.save(planta);

    }

    public List<Planta> getTodosEmEstoque() {
        return plantaRepository.findAllByQtdEmEstoqueGreaterThan(0);
    }

    public Planta buscarPorId(Long id) {
        return plantaRepository.getOne(id);
    }

    public void excluirPlanta(Long id) {
        plantaRepository.deleteById(id);
    }
}
