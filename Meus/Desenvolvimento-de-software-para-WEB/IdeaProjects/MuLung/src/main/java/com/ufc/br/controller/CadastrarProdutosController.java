package com.ufc.br.controller;
import com.ufc.br.model.Enfeite;
import com.ufc.br.model.Ferramenta;
import com.ufc.br.model.Planta;
import com.ufc.br.model.Servico;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class CadastrarProdutosController {

    @RequestMapping("/Cadastrar/Produto")
    public ModelAndView cadastrar(String mensagem){
        ModelAndView mv = new ModelAndView("cadastrarProdutos");
        mv.addObject("ferramenta", new Ferramenta());
        mv.addObject("planta", new Planta());
        mv.addObject("enfeite", new Enfeite());
        mv.addObject("servico", new Servico());

        mv.addObject("mensagem", mensagem);

        return mv;
    }
}
