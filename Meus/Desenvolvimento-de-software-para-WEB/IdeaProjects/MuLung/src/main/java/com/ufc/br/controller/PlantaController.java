package com.ufc.br.controller;


import com.ufc.br.Service.PlantaService;
import com.ufc.br.model.Planta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PlantaController {

    @Autowired
    private PlantaService plantaService;

    @RequestMapping("planta/salvar")
    public ModelAndView salvar(Planta planta, @RequestParam(value = "imagempla")MultipartFile imagem){
        plantaService.salvar(planta, imagem);
        CadastrarProdutosController cadastrarProdutosController = new CadastrarProdutosController();
        return cadastrarProdutosController.cadastrar("Planta cadastrada com sucesso");
    }

    @RequestMapping("planta/editar/{id}")
    public ModelAndView editar(@PathVariable Long id){
        Planta planta = plantaService.buscarPorId(id);
        ModelAndView mv = new ModelAndView("editarPlanta");
        mv.addObject("planta", planta);
        return mv;
    }

    @RequestMapping("planta/excluir/{id}")
    public ModelAndView excluir(@PathVariable Long id){
        plantaService.excluirPlanta(id);
        ModelAndView mv = new ModelAndView("redirect:/Produtos");
        return mv;
    }
}
