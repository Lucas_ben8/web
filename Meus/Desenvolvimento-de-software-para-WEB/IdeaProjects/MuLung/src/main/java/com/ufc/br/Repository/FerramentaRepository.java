package com.ufc.br.Repository;

import com.ufc.br.model.Ferramenta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FerramentaRepository extends JpaRepository<Ferramenta, Long> {

    List<Ferramenta> findAllByQtdEmEstoqueGreaterThan(int n);

    //List<Ferramenta> findAllByNomeContainingAndQtdEmEstoqueGreaterThan(String a, int b); // esse daqui vai ser usado pra pesquisar na pagina

    //mas ainda não sei se vou usar
}
