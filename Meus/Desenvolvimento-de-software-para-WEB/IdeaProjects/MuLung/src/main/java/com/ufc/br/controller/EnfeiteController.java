package com.ufc.br.controller;

import com.ufc.br.Service.EnfeiteService;
import com.ufc.br.model.Enfeite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EnfeiteController {

    @Autowired
    private EnfeiteService enfeiteService;

    @RequestMapping("/enfeite/salvar")
    public ModelAndView salvar(Enfeite enfeite, @RequestParam(value = "imagemenf")MultipartFile imagem){
        enfeiteService.salvar(enfeite, imagem);
        CadastrarProdutosController cadastrarProdutosController = new CadastrarProdutosController();
        return cadastrarProdutosController.cadastrar("Enfeite cadastrado com Sucesso!");
    }

    @RequestMapping("/enfeite/editar/{id}")
    public ModelAndView editar(@PathVariable Long id){
        Enfeite enfeite = enfeiteService.buscarPorId(id);
        ModelAndView mv = new ModelAndView("editarEnfeite");
        mv.addObject(enfeite);
        return mv;
    }

    @RequestMapping("/enfeite/excluir/{id}")
    public ModelAndView excluir(@PathVariable Long id){
        enfeiteService.excluir(id);
        ModelAndView mv = new ModelAndView("redirect:/Produtos");
        return mv;
    }
}
