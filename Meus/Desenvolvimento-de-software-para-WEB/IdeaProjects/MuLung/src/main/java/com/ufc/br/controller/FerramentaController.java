package com.ufc.br.controller;

import com.ufc.br.Service.FerramentaService;
import com.ufc.br.model.Ferramenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FerramentaController {

    @Autowired
    private FerramentaService ferramentaService;

    @RequestMapping("/ferramenta/salvar")
    public ModelAndView salvar(Ferramenta ferramenta, @RequestParam(value = "imagemfer")MultipartFile imagem){
        ferramentaService.salvar(ferramenta, imagem);
        CadastrarProdutosController cadastrarProdutosController =  new CadastrarProdutosController();
        return cadastrarProdutosController.cadastrar("Ferramenta cadastrada com Sucesso!");
    }

    @RequestMapping("/ferramenta/editar/{id}")
    public ModelAndView editar(@PathVariable Long id){
        Ferramenta ferramenta = ferramentaService.buscarPorId(id);
        ModelAndView mv = new ModelAndView("editarFerramenta");
        mv.addObject("ferramenta", ferramenta);
        return mv;
    }

    @RequestMapping("/ferramenta/excluir/{id}")
    public ModelAndView excluir(@PathVariable Long id){
        ferramentaService.excluirFerramenta(id);
        ModelAndView mv = new ModelAndView("redirect:/Produtos");
        return mv;
    }




}
