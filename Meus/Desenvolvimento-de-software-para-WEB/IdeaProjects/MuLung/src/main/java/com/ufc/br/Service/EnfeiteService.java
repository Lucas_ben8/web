package com.ufc.br.Service;

import com.ufc.br.Repository.EnfeiteRepository;
import com.ufc.br.Util.ArquivoUtil;
import com.ufc.br.model.Enfeite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class EnfeiteService {

    @Autowired
    private EnfeiteRepository enfeiteRepository;

    public void salvar(Enfeite enfeite, MultipartFile imagem){
        String caminho = "Imagens/img/" + enfeite.getNome() + "_" + enfeite.getDescricao() + ".png";
        ArquivoUtil.salvarImagem(caminho, imagem);
        enfeiteRepository.save(enfeite);
    }

    public List<Enfeite> getTodosEmEstoque(){
        return enfeiteRepository.findAllByQtdEmEstoqueGreaterThan(0);
    }

    public Enfeite buscarPorId(Long id) {
        return enfeiteRepository.getOne(id);
    }

    public void excluir(Long id) {
        enfeiteRepository.deleteById(id);
    }
}
