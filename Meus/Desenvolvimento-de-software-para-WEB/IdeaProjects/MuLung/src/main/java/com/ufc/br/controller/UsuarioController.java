package com.ufc.br.controller;

import com.ufc.br.Service.UsuarioService;
import com.ufc.br.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @RequestMapping("/usuario/formulario")
    public ModelAndView formulario(){
        ModelAndView mv = new ModelAndView("formulario");
        mv.addObject("usuario", new Usuario());
        return mv;
    }

    @RequestMapping("/usuario/salvar")
    public ModelAndView salvar(Usuario usuario){
        usuarioService.salvar(usuario);
        ModelAndView mv = new ModelAndView("redirect:/PaginaPrincipal");
        return mv;
    }

    @RequestMapping("usuario/logar")
    public String logar(){
        return "paginaLogin";
    }

    @RequestMapping("usuario/perfil/{user}")
    public ModelAndView perfil(@PathVariable String user){
        Usuario usuario = usuarioService.buscarPorUsername(user);
        ModelAndView mv = new ModelAndView("perfil");
        mv.addObject("perfilU", usuario);
        return mv;
    }

    @RequestMapping("usuario/editar/{id}")
    public ModelAndView editar(@PathVariable Long id){
        Usuario usuario = usuarioService.buscaPorId(id);
        ModelAndView mv = new ModelAndView("formulario");
        mv.addObject("usuario", usuario);
        return mv;
    }

    @RequestMapping("usuario/excluir/{id}")
    public ModelAndView excluir(@PathVariable Long id){
        usuarioService.deletarPorId(id);
        ModelAndView mv = new ModelAndView("redirect:/logout");
        return mv;
    }

    @RequestMapping("/logout")
    public ModelAndView logout(){
        ModelAndView mv = new ModelAndView("paginaLogin");
        return mv;
    }




}
