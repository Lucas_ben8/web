package com.ufc.br.Repository;

import com.ufc.br.model.Enfeite;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnfeiteRepository extends JpaRepository<Enfeite, Long> {

    List<Enfeite> findAllByQtdEmEstoqueGreaterThan(int n);
}
