package com.ufc.br.Service;

import com.ufc.br.Repository.FerramentaRepository;
import com.ufc.br.Util.ArquivoUtil;
import com.ufc.br.model.Ferramenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class FerramentaService {


    @Autowired
    private FerramentaRepository ferramentaRepository;

    public void salvar(Ferramenta ferramenta, MultipartFile imagem) {
        String caminho = "Imagens/img/" + ferramenta.getNome() + "_" + ferramenta.getMarca() + ".png";
        ArquivoUtil.salvarImagem(caminho, imagem);
        ferramentaRepository.save(ferramenta);
    }

    public List<Ferramenta> getTodosEmEstoque() {
        return ferramentaRepository.findAllByQtdEmEstoqueGreaterThan(0);
    }

    public Ferramenta buscarPorId(Long id) {
        return ferramentaRepository.getOne(id);
    }

    public void excluirFerramenta(Long id) {
        ferramentaRepository.deleteById(id);
    }
}
