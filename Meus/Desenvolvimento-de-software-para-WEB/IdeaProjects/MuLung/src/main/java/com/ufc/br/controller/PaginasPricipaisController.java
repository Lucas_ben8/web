package com.ufc.br.controller;

import com.ufc.br.Service.EnfeiteService;
import com.ufc.br.Service.FerramentaService;
import com.ufc.br.Service.PlantaService;
import com.ufc.br.Service.ServicoService;
import com.ufc.br.model.Enfeite;
import com.ufc.br.model.Ferramenta;
import com.ufc.br.model.Planta;
import com.ufc.br.model.Servico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Controller
public class PaginasPricipaisController {

    @Autowired
    private FerramentaService ferramentaService;

    @Autowired
    private PlantaService plantaService;

    @Autowired
    private EnfeiteService enfeiteService;

    @Autowired
    private ServicoService servicoService;

    @RequestMapping("/PaginaPrincipal")
    public ModelAndView paginaPricipal(){
        ModelAndView mv = new ModelAndView("PaginaPrincipal");
        List<Ferramenta> ferramentas = ferramentaService.getTodosEmEstoque();
        Collections.shuffle(ferramentas);
        List<Ferramenta> ferramentas1 = new ArrayList<>();
        for(int i = 0; i < 2 && i < ferramentas.size(); i++){
            ferramentas1.add(ferramentas.get(i));
        }
        mv.addObject("TodasAsferramentas", ferramentas1);

        List<Planta> plantas = plantaService.getTodosEmEstoque();
        Collections.shuffle(plantas);
        List<Planta> plantas1 = new ArrayList<>();
        for(int i = 0; i < 2 && i < plantas.size(); i++){
            plantas1.add(plantas.get(i));
        }
        mv.addObject("TodasAsPlantas", plantas1);

        List<Enfeite> enfeites = enfeiteService.getTodosEmEstoque();
        Collections.shuffle(enfeites);
        List<Enfeite> enfeites1 = new ArrayList<>();
        for(int i = 0; i < 2 && i < enfeites.size(); i++){
            enfeites1.add(enfeites.get(i));
        }
        mv.addObject("TodosOsEnfeites", enfeites1);

        List<Servico> servicos = servicoService.getTodos();
        Collections.shuffle(servicos);
        List<Servico> servicos1 = new ArrayList<>();
        for( int i = 0; i < 2 && i < servicos.size(); i++){
            servicos1.add(servicos.get(i));
        }
        mv.addObject("TodosOsServicos", servicos1);
        return mv;
    }

    @RequestMapping("/Sobre")
    public ModelAndView sobre(){
        ModelAndView mv = new ModelAndView("Sobre");
        return mv;
    }

    @RequestMapping("/Produtos")
    public ModelAndView produtos(){
        ModelAndView mv = new ModelAndView("Produtos");
        List<Ferramenta> ferramentas = ferramentaService.getTodosEmEstoque();
        mv.addObject("TodasAsferramentas", ferramentas);
        List<Planta> plantas = plantaService.getTodosEmEstoque();
        mv.addObject("TodasAsPlantas", plantas);
        List<Enfeite> enfeites = enfeiteService.getTodosEmEstoque();
        mv.addObject("TodosOsEnfeites", enfeites);
        List<Servico> servicos = servicoService.getTodos();
        mv.addObject("TodosOsServicos", servicos);
        return mv;
    }

}
