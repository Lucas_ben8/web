package com.ufc.br.config;


import com.ufc.br.Security.UserDetailsServiceImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImplement userDetailsServiceImplement;

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.csrf().disable().authorizeRequests()
//                .anyRequest().permitAll();
                .antMatchers("/PaginaPrincipal").permitAll()
                .antMatchers("/Produtos").permitAll()
                .antMatchers("/usuario/formulario").permitAll()
                .antMatchers("/Sobre").permitAll()
                .antMatchers("/usuario/salvar").permitAll()

                .antMatchers("/usuario/perfil").hasAnyRole("USER", "ADMIN")



                .antMatchers("/servico/salvar").hasRole("ADMIN")
                .antMatchers("/servico/editar").hasRole("ADMIN")
                .antMatchers("/planta/salvar").hasRole("ADMIN")
                .antMatchers("/planta/editar").hasRole("ADMIN")
                .antMatchers("/enfeite/salvar").hasRole("ADMIN")
                .antMatchers("/enfeite/editar").hasRole("ADMIN")
                .antMatchers("/ferramenta/salvar").hasRole("ADMIN")
                .antMatchers("/ferramenta/editar").hasRole("ADMIN")

                .anyRequest().authenticated()
                .and().formLogin().loginPage("/usuario/logar").permitAll()
                .and().logout().logoutSuccessUrl("/usuario/logar?logout").permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(userDetailsServiceImplement).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) throws Exception{
        web.ignoring().antMatchers("/CSS/**", "/JS/**", "/Imagens/**");
    }


}
