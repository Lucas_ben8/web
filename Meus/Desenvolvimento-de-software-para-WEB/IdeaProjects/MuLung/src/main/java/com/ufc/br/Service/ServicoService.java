package com.ufc.br.Service;

import com.ufc.br.Repository.ServicoRepository;
import com.ufc.br.Util.ArquivoUtil;
import com.ufc.br.model.Ferramenta;
import com.ufc.br.model.Servico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class ServicoService {

    @Autowired
    public ServicoRepository servicoRepository;

    public void salvar(Servico servico, MultipartFile imagem) {
        String caminho = "Imagens/img/" + servico.getNome() + "_" + servico.getTipo() + ".png";
        ArquivoUtil.salvarImagem(caminho, imagem);
        servicoRepository.save(servico);
    }

    public List<Servico> getTodos() {
        return servicoRepository.findAll();
    }

    public Servico buscarPorId(Long id) {
        return servicoRepository.getOne(id);
    }

    public void excluirServico(Long id) {
        servicoRepository.deleteById(id);
    }
}
