package com.ufc.br.Service;

import com.ufc.br.Repository.UsuarioRepository;
import com.ufc.br.model.Usuario;
import org.hibernate.dialect.MySQL57Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;


    public void salvar(Usuario usuario) {
        usuario.setSenha(new BCryptPasswordEncoder().encode(usuario.getSenha()));
        usuarioRepository.save(usuario);
    }

    public Usuario buscarPorUsername(String user) {
        return usuarioRepository.findByLogin(user);
    }

    public Usuario buscaPorId(Long id) {
        return usuarioRepository.getOne(id);
    }

    public void deletarPorId(Long id) {
        usuarioRepository.deleteById(id);
    }
}
